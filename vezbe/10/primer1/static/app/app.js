import Prodavnica from './components/prodavnica.js'
import TabelaKorisnika from './components/tabelaKorisnika.js'
import KorisnikForma from './components/korisnikForma.js'
import TabelaProizvoda from './components/tabelaProizvoda.js';

const app = Vue.createApp(Prodavnica);
app.component('tabela-korisnika', TabelaKorisnika);
app.component('tabela-proizvoda', TabelaProizvoda);
app.component('korisnik-forma', KorisnikForma);
app.mount("#app");