import socket
import sys

def parse_request(data):      #   GET /index.html HTTP 1.0
    data = data.decode()
    lines = data.split('\n')
    for line in lines:
        print(line)
    
    method, url, version = lines[0].split(' ')
    print('method:', method)
    print('url:', url)
    print('version', version)
    return method, url

def prepare_response(contents):
    status = b"HTTP/1.0 200 OK\r\n"
    headers = []
    headers.append(b"Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n")
    headers.append(b"Server: Apache/0.8.4\r\n")
    headers.append()

    length = 0
    for line in contents:
        length += len(line)

    headers.append(b"Content-Type: text/html\r\n")
    headers.append(("Content-Length: {}\r\n".format(length)).encode())
    headers.append(b"Expires: Sat, 01 Jan 2000 00:59:59 GMT\r\n")
    headers.append(b"Last-modified: Fri, 09 Aug 1996 14:21:40 GMT\r\n")
    headers.append(b"Connection: close\r\n")
    
    return {
        'status':status,
        'headers': headers,
        'content': contents
    }


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ('localhost', 8080)
    print("server pokrenut")
    sock.bind(server_address)

    sock.listen(0)

    while True:
        print("waiting for a connection")
        connection, client_address = sock.accept()
        print("connection from {}".format(client_address))
        data = connection.recv(1024)
        method, url = parse_request(data)

        contents = []
        if url == '/index.html':
            # priprema sadrzaja
            contents = [b"<TITLE>Example</TITLE>", b"<html><body><P>Primer 1</P>"]
            for i in range(20):
                contents.append(('{}<br>'.format(i)).encode())
            contents.append(b'</body></html>')

        if url == '/korisnici.html':
            # priprema sadrzaja
            contents = [b"<TITLE>Korisnici</TITLE>", b"<html><body><P>Lista korisnika</P>"]
            for i in range(20):
                contents.append(('{}<br>'.format(i)).encode())
            contents.append(b'</body></html>')

        # formatiranje za slanje
        data = prepare_response(contents)
        # slanje
        connection.sendall(data['status'])
        for header in data['headers']:
            connection.sendall(header)
        connection.sendall(b"\r\n")
        for line in data['content']:
            connection.sendall(line)
        connection.close()
main()