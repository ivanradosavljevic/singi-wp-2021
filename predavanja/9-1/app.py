from flask import Flask, render_template, request, redirect
import pymysql.cursors
import json
import drzava
import mesto

app = Flask(__name__, static_folder="static", static_url_path="/")

conn = pymysql.connect( host='localhost',
                        user='root',
                        password='levo.desno',
                        database='wp',
                        charset='utf8mb4',
                        cursorclass=pymysql.cursors.DictCursor)

drzava.init(app, conn)
mesto.init(app, conn)

@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route('/<path:path>')  
def send_file(path):
    return app.send_static_file(path)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, )