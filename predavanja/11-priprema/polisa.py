import json
from flask import request
import datetime
'''
  polisa
  `automobil_id` INT NOT NULL,
  `osiguravajuca_kuca_id` INT NOT NULL,
  `datum_pocetka` DATETIME NOT NULL,
  `datum_kraja` DATETIME NOT NULL,
  `cena` DECIMAL NOT NULL,
'''
conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/api/polisa', view_func=polisa_create, methods=['POST'])
    app.add_url_rule('/api/polisa', view_func=polisa_read, methods=['GET'])
    app.add_url_rule('/api/polisa/<int:id>', view_func=polisa_update, methods=['PUT'])
    app.add_url_rule('/api/polisa/<int:id>', view_func=polisa_delete, methods=['DELETE'])

def polisa_create():
    with conn.cursor() as cursor:
        # datum_pocetka = datetime.datetime(2009,5,5)
        # str_datum_pocetka = datum_pocetka.date().isoformat()
        # datum_kraja = datetime.datetime(2009,5,5)
        # str_datum_kraja = datum_kraja.date().isoformat()
        obj = request.json
        print('datum_pocetka', obj['datum_pocetka'])
        print('datum_kraja', obj['datum_kraja'])
        print(obj)

        # obj['datum_pocetka'] = str_datum_pocetka
        # obj['datum_kraja'] = str_datum_kraja

        cursor.execute("INSERT INTO polisa(automobil_id, osiguravajuca_kuca_id, datum_pocetka, datum_kraja, cena) VALUES (%(automobil_id)s, %(osiguravajuca_kuca_id)s, %(datum_pocetka)s, %(datum_kraja)s, %(cena)s)", obj)
    conn.commit()
    return 'ok', 201

def polisa_read():
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT id, automobil_id, osiguravajuca_kuca_id, datum_pocetka, datum_kraja, cena FROM polisa")
        for el in cursor.fetchall():
            el['datum_pocetka'] = el['datum_pocetka'].isoformat()
            el['datum_kraja'] = el['datum_kraja'].isoformat()
            el['cena'] = float(el['cena'])
            ret.append(el)
    return json.dumps(ret), 200

def polisa_update(id):
    polisa = dict(request.json)
    polisa['id'] = id
    with conn.cursor() as cursor:
        cursor.execute("UPDATE polisa SET automobil_id= %(automobil_id)s, osiguravajuca_kuca_id= %(osiguravajuca_kuca_id)s, datum_pocetka= %(datum_pocetka)s, datum_kraja= %(datum_kraja)s, cena= %(cena)s WHERE id = %(id)s", polisa)
    conn.commit()
    return 'ok', 201

def polisa_delete(id):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM polisa WHERE id = %s", (id,))
    conn.commit()
    return 'ok', 201

