import json
from flask import request
'''
  osiguravajuca_kuca
  `naziv` TEXT NOT NULL,
'''
conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/api/osiguravajuca_kuca', view_func=osiguravajuca_kuca_create, methods=['POST'])
    app.add_url_rule('/api/osiguravajuca_kuca', view_func=osiguravajuca_kuca_read, methods=['GET'])
    app.add_url_rule('/api/osiguravajuca_kuca/<int:id>', view_func=osiguravajuca_kuca_update, methods=['PUT'])
    app.add_url_rule('/api/osiguravajuca_kuca/<int:id>', view_func=osiguravajuca_kuca_delete, methods=['DELETE'])

def osiguravajuca_kuca_create():
    with conn.cursor() as cursor:
        cursor.execute("INSERT INTO osiguravajuca_kuca(naziv) VALUES (%(naziv)s)", request.json)
    conn.commit()
    return 'ok', 201

def osiguravajuca_kuca_read():
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT id, naziv FROM osiguravajuca_kuca")
        for el in cursor.fetchall():
            ret.append(el)
    return json.dumps(ret), 200

def osiguravajuca_kuca_update(id):
    osiguravajuca_kuca = dict(request.json)
    osiguravajuca_kuca['id'] = id
    with conn.cursor() as cursor:
        cursor.execute("UPDATE osiguravajuca_kuca SET naziv = %(naziv)s WHERE id = %(id)s", osiguravajuca_kuca)
    conn.commit()
    return 'ok', 201

def osiguravajuca_kuca_delete(id):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM osiguravajuca_kuca WHERE id = %s", (id,))
    conn.commit()
    return 'ok', 201

