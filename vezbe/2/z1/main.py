import flask
from flask import Flask

app = Flask(__name__, static_folder="static", static_url_path="/")

studenti = [{"indeks": "1234/123456", "ime": "Ime", "prezime": "Prezime", "ocena": 10}]

@app.route("/")
def home():
    studenti_html = []
    for s in studenti:
        studenti_html.append('''
            <tr>
                <td><a href="student?indeks={indeks}">{indeks}</a></td>
                <td>{ime}</td>
                <td>{prezime}</td>
                <td>{ocena}</td>
                <td><a href="ukloniStudenta?indeks={indeks}">Ukloni</a>
                <a href="izmeniStudentaForma?indeks={indeks}">Izmeni</a></td>
            </tr>'''.format(**s))
        
    studenti_html = "".join(studenti_html)
    return '''
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Document</title>
    </head>
    <body>
        <a href="/forma.html">Unos studenta</a>
        <table>
            <tr>
                <th>Broj indeksa</th>
                <th>Ime</th>
                <th>Prezime</th>
                <th>Prosecna ocena</th>
                <th>Akcije</th>
            </tr>
            {}
        </table>
    </body>
    </html>
    '''.format(studenti_html)

@app.route("/dodajStudenta", methods=["POST"])
def dodaj_studenta():
    studenti.append(flask.request.form)
    return flask.redirect("/")

@app.route("/student", methods=["GET"])
def dobavi_studenta():
    indeks = flask.request.args["indeks"]
    for s in studenti:
        if s["indeks"] == indeks:
            return '''
                    <!DOCTYPE html>
                    <html lang="en">

                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <script type="text/javascript" src="script.js" defer></script>
                        <title>Document</title>
                    </head>

                    <body>
                        <ul>
                            <li>{indeks}</li>
                            <li>{ime}</li>
                            <li>{prezime}</li>
                            <li>{ocena}</li>
                        </ul>
                    </body>

                    </html>
                    '''.format(**s)
    return app.send_static_file("notFound.html")

@app.route("/ukloniStudenta", methods=["GET"])
def ukloni_studenta():
    for i, s in enumerate(studenti):
        if s["indeks"] == flask.request.args["indeks"]:
            studenti.pop(i)
    return flask.redirect("/")

@app.route("/izmeniStudentaForma", methods=["GET"])
def izmeni_studenta_forma():
    for i, s in enumerate(studenti):
        if s["indeks"] == flask.request.args["indeks"]:
            return '''
                <!DOCTYPE html>
                <html lang="en">

                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script type="text/javascript" src="script.js" defer></script>
                    <title>Document</title>
                </head>

                <body>
                    <form action="/izmeniStudenta?indeks={indeks}" method="POST" onsubmit="return validacija()">
                        <div>
                            <label for="indeks">Indeks</label>
                            <input name="indeks" id="indeks" type="text" value="{indeks}" required />
                        </div>
                        <div>
                            <label for="ime">Ime</label>
                            <input name="ime" id="ime" type="text" value="{ime}" required />
                        </div>
                        <div>
                            <label for="prezime">Prezime</label>
                            <input name="prezime" id="prezime" type="text" value={prezime} required />
                        </div>
                        <div>
                            <label for="ocena">Prosecna ocena</label>
                            <input name="ocena" id="ocena" type="number" min="5" max="10" value={ocena} required />
                        </div>
                        <div>
                            <input type="submit" value="Izmeni">
                        </div>
                    </form>
                </body>

                </html>
            '''.format(**s)
    return flask.redirect("notFound.html")

@app.route("/izmeniStudenta", methods=["POST"])
def izmeni_studenta():
    for i, s in enumerate(studenti):
        if s["indeks"] == flask.request.args["indeks"]:
            studenti[i] = flask.request.form
    return flask.redirect("/")

if __name__ == "__main__":
    app.run()