var UserForm = {
    props:{
        title:String
    },
    data: function(){
        return {
            username:"",
            password:""
        }
    },
    template: 
    `<div class="user-form">
        Username: <input v-model="username"> {{username}}<br>
        Password: <input type="password" v-model="password">{{password}}
    </div>`
}
export default UserForm;