export var AppStudentComponent = {

    template:`

    <div>
    <b-card title="Student">
      <b-card-text>
      
      <b-button v-b-modal.modal-1>Launch demo modal</b-button>

      <b-modal id="modal-1" title="BootstrapVue">
        <p class="my-4">Hello from modal!</p>
      </b-modal>

      
      <b-button variant="outline-primary" v-on:click="create()">Create</b-button>
      <b-button v-on:click="update()">Update</b-button><br>

      <b-row>
        <b-col col lg="8"  xs="12">
        <form>
        <b-form-group  id="input-group-1"
        label="Ime:"
        label-for="input-1"
        description="Ime studenta">
        
        <b-form-input
        id="input-1"
        v-model="izabran_student.ime"
        type="text"
        placeholder="Ime"
        required></b-form-input>
      </b-form-group>

      <b-form-group  id="input-group-2"
        label="Prezime:"
        label-for="input-2"
        description="Prezime studenta">
        
        <b-form-input
        id="input-2"
        v-model="izabran_student.prezime"
        type="text"
        placeholder="Prezime"
        required></b-form-input>
      </b-form-group>

      <b-form-group  id="input-group-3"
      label="Indeks:"
      label-for="input-3"
      description="Indeks studenta">
      
      <b-form-input
      id="input-3"
      v-model="izabran_student.indeks"
      type="text"
      placeholder="Indeks"
      required></b-form-input>
    </b-form-group>
      </form>



        </b-col>
        <b-col>2 of 3</b-col>
       </b-row>



      <div v-for="student in studenti">
          <a href="#" v-on:click="select(student)">{{student.ime}} {{student.prezime}}</a>
          <a href="#" v-on:click="delete_student(student.id)">delete</a>
      </div>

      <b-calendar v-model="datumIspita" locale="en-US"></b-calendar>
        {{datumIspita}}



      </b-card-text>
    </b-card>
  </div>
    `,
    data:function(){
        return {
            studenti:[],
            izabran_student:{
                imde:'',
                prezime:'',
                indeks:''
            },
            datumIspita:''
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(student){
            this.izabran_student = {...student}
        },
        create:function(){
            axios.post("/api/student", this.izabran_student).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function(){
            axios.get("/api/student").then(
                response =>{
                    this.studenti = response.data;
                    if(this.studenti.length>0){
                        this.izabran_student = {...this.studenti[0]};
                    }
                } 
            )
        },
        update:function(){
            axios.put("/api/student/"+this.izabran_student.id, this.izabran_student).then(
                response =>{
                    this.read();
                } 
            )
        },
        delete_student:function(id){
            axios.delete("/api/student/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    }

}