def servis(studenti):
    for student in studenti:
        prosek = 0
        for ocena in student['ocene']:
            prosek += ocena
        prosek = prosek/len(student['ocene'])
        student['prosek'] = prosek
    return studenti

