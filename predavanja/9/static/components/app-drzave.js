export var AppDrzave = {
    template:`
        <div>
            <h2> Drzave </h2>
            <div v-for="drzava in drzave">
                <a href="#" v-on:click="select(drzava)">{{drzava.naziv}}</a><a href="#" v-on:click="ddelete(drzava.id)">-</a>
            </div>
            <input v-model="drzava.naziv" placeholde="Naziv drzave">
            <button v-on:click="create()"> Create </button>
            <button v-on:click="update()"> Update </button>
            <app-mesta :drzava="drzava"/>
            <br>
            

        </div>
    `,
    data:function(){
        return {
            naziv:'',
            drzava:{},
            drzave: []
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(drzava){
            this.drzava = {...drzava};
        },
        create:function(){
            axios.post("/drzave", {naziv:this.drzava.naziv}).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function (){
            axios.get("/drzave").then(
                response =>{
                    this.drzave = response.data;
                    this.drzava = {...this.drzave[0]};
                } 
            )
        },
        update:function(){
            axios.put("/drzave/"+this.drzava.id, this.drzava).then(
                response =>{
                    this.read();
                } 
            )
        },
        ddelete:function(id){
            axios.delete("/drzave/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    },
};