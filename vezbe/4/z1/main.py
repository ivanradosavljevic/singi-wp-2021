import flask
from flask import Flask

from flaskext.mysql import MySQL
from flaskext.mysql import pymysql

app = Flask(__name__, static_url_path="")

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "studentska_sluzba"

mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

@app.route("/")
def home():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM student")
    return flask.render_template("index.tpl.html", studenti=cursor.fetchall())

@app.route("/dodajStudenta", methods=["POST"])
def dodaj_studenta():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO student(brojIndeksa, ime, prezime, email) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(email)s)", flask.request.form)
    db.commit()
    return flask.redirect("/")

@app.route("/izmeniStudenta", methods=["GET"])
def izmeni_studenta_forma():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM student WHERE brojIndeksa=%s", (flask.request.args["brojIndeksa"], ))
    return flask.render_template("izmeniStudentaForma.html", student=cursor.fetchone())

@app.route("/izmeniStudenta", methods=["POST"])
def izmeni_studenta():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE student SET brojIndeksa=%(brojIndeksa)s, ime=%(ime)s, prezime=%(prezime)s, email=%(email)s WHERE brojIndeksa=%(brojIndeksa)s", flask.request.form)
    db.commit()
    return flask.redirect("/")

@app.route("/ukloniStudenta")
def ukloni_studenta():
    broj_indeksa = flask.request.args["brojIndeksa"]
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM student WHERE brojIndeksa=%s", (broj_indeksa, ))
    db.commit()
    return flask.redirect("/")

if __name__ == "__main__":
    app.run()