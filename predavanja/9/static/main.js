import { AppDrzave } from "/components/app-drzave.js";
import { AppMesta } from "/components/app-mesta.js";


Vue.component('app-drzave', AppDrzave);
Vue.component('app-mesta', AppMesta);


var app = new Vue({
    el: "#app",
    template:`
        <div>
            Danas je {{title}}<br>
            <!-- <app-sum v-bind:naslov="title"></app-sum> -->
            <!-- <app-exchange kursEvra=118.5></app-exchange> -->
            <app-drzave></app-drzave>
        </div>
    `,
    data:{
        title:"sreda 21-04-2021"
    }
})