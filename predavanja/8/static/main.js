import TodoItem from '/components/todo.js';
import UserForm from '/components/user-form.js';

Vue.component('todo-item', TodoItem);
Vue.component('user-form', UserForm);


var app = new Vue({
    el: "#app",
    template:`
        <div>
            <div v-for="user in users">
            {{user}},
            </div>
            <todo-item title="a1"> </todo-item>
            <todo-item title="a2"> </todo-item>
            <todo-item title="a3"> </todo-item>
            <todo-item :title="poruka"> </todo-item>
            
            <user-form></user-form>
        </div>
    `,
    data: {
        poruka: "Hello",
        users:[]
    },
    created: function () {
        // this.$http.get('/users').then(
        //     response=>this.users = response.data
        // )
        axios.get('/users').then(
            response=>this.users = response.data
        )

    }
})