#!/usr/bin/env python3

from jinja2 import Template

lista= [1, 3, 2, -1, 4]

template = """
{% for el in obj -%}
    {{ el }}
    {% if el < 0 %}
        ---------
    {% endif %} 
{% endfor %}
"""

tm = Template(template)
msg = tm.render(obj=lista)

print(msg)