import json
from flask import request

conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/drzave',          view_func=drzava_create, methods=['POST'])
    app.add_url_rule('/drzave',          view_func=drzava_read,   methods=['GET'])
    app.add_url_rule('/drzave/<int:id>', view_func=drzava_update, methods=['PUT'])
    app.add_url_rule('/drzave/<int:id>', view_func=drzava_delete, methods=['DELETE'])


def drzava_create():
    with conn.cursor() as cursor:
        cursor.execute("INSERT INTO drzava(naziv) VALUES(%(naziv)s)", request.json)
    conn.commit()
    return request.json, 201

def drzava_read():
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT * FROM drzava")
        for el in cursor.fetchall():
            ret.append(el)
    return json.dumps(ret)

def drzava_update(id):
    drzava = dict(request.json)
    drzava["id"] = id
    with conn.cursor() as cursor:
        cursor.execute("UPDATE drzava SET naziv=%(naziv)s WHERE id=%(id)s", drzava)
        conn.commit()
        cursor.execute("SELECT * FROM drzava WHERE id=%s", (id,))
        obj = cursor.fetchone()
        return json.dumps(obj)
    return "fail", 500

def drzava_delete(id):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM drzava WHERE id=%(id)s", {"id":id})
        conn.commit()
    return "ok", 200


