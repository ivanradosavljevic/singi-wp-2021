"""
CREATE TABLE users (
    id int NOT NULL AUTO_INCREMENT,
    email varchar(255),
    password varchar(255),
    PRIMARY KEY (id)
);
"""
import pymysql.cursors

# Connect to the database
conn = pymysql.connect(host='localhost',
                            user='root',
                            password='levo.desno',
                            database='primer1',
                            charset='utf8mb4',
                            cursorclass=pymysql.cursors.DictCursor)

def primer_insert(conn):
    with conn.cursor() as cursor:
        sql = "INSERT INTO users (email, password) VALUES (%s, %s)"
        cursor.execute(sql, ('djobradovic@singidunum.ac.rs', 'kifla.krofna'))
    conn.commit()

def primer_select(conn):
    with conn.cursor() as cursor:
        # Read a single record
        sql = "SELECT id, password FROM users WHERE email=%s"
        cursor.execute(sql, ('djobradovic@singidunum.ac.rs',))
        result = cursor.fetchone()
        print(result)

def primer_update(conn):
    cursor = conn.cursor()
    sql = "UPDATE users SET password = %s WHERE email=%s"
    cursor.execute(sql, ('krofna.kifla', 'djobradovic@singidunum.ac.rs'))
    cursor.close()
    conn.commit()

def primer_delete(conn):
    cursor = conn.cursor()
    sql = "DELETE FROM users WHERE email=%s"
    cursor.execute(sql, ('djobradovic@singidunum.ac.rs'))
    cursor.close()
    conn.commit()

primer_insert(conn)
primer_select(conn)
# primer_update(conn)
# primer_select(conn)
# primer_delete(conn)
# primer_select(conn)