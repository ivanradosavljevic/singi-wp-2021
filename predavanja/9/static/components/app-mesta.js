export var AppMesta = {
    template:`
        <div>
            <h2> Mesta </h2>
            <div v-for="mesto in mesta">
                {{mesto.naziv}}
            </div>
        </div>
    `,
    props:['drzava'],
    data:function(){
        return {
            mesta: [],
        }
    },
    watch:{
        drzava:function(newVal, oldVal) {
            this.read();
        }
    },
    methods:{
        read:function (){
            axios.get("/mesta/"+this.drzava.id).then(
                response =>{
                    this.mesta = response.data;
                } 
            )
        }
    },
};