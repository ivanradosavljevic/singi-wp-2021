# Vežbe 3
*Cilj vežbi: Generisanje sadržaja na serverskoj strani upotrebom Jinja2 šablona.*

## Zadaci
1. Prepraviti web aplikaciju sa prethodnih vežbi tako da se za generisanje stranica koriste Jinja2 šabloni.
2. Prepraviti da se brisanje studenata vrši logički, odnosno proširiti opis studenata uveđenjem atributa obrisan koji može imati vrednost true ili false. Obrisane studente prikazati u tabeli kao redove koji imaju crvenu pozadinu.
3. Napraviti web aplikaciju za upravljanje robom. Roba sadrži atribute id, naziv, proizvođač i cena. Omogućiti tabelaran prikaz robe, prikaz pojedinačnih stavki iz tabele, brisanje, izmenu i dodavanje robe.
4. Napraviti web aplikaciju za prodaju bioskopskih karata. Karta je opisana atributima id, naziv projekcije, datum i vreme početka, datum i vreme završetka i broj sedišta. Omogućiti tabelarni prikaz karata, dodavanje, brisanje i izmenu karata. Prilikom dodavanja i izmene karata onemogućiti da karte za različite projekcije imaju preklapanje po datumu i vremenu. U slučaju preklapanja obavestiti korisnika porukom prikazanom na odvojenoj stranici.
___
### Dodatne napomene:
* Dokumentacija za Flask radni okvir: https://palletsprojects.com/p/flask/.
___