export var AppUsers = {
    template:`
        <div class="users">
            <h2> korisnici </h2>
            <div v-for="user in users">
                {{user.email}}
            </div>
            <button v-on:click="getUsers()"> Preuzmi korisnike </button>
        </div>
    `,
    data:function(){
        return {
            users: []
        }
    },
    methods:{
        getUsers:function (){
            // this.users = ['djordje', 'marko', 'milan'];
            axios.get("/users").then(
                response => this.users = response.data
            )
        }
    }
};