import json

def init(app):
    app.add_url_rule('/users', view_func=index, methods=['GET'])

def index():
    users = [
        'petar',
        'milica',
        'marko'
    ]
    return json.dumps(users)
