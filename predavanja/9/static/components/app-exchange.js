export var AppExchangeComponent = {
    props:{
        kursEvra:Number
    },
    template:`
        <div>
            <h2> Menjacnica </h2>
            Iznos dinara <input v-model="dinari"><br>
            Iznos Evra <input v-model="evri"><br>
            <button v-on:click="pretvoriUDinare()">EUR->RSD</button>
            <button v-on:click="pretvoriUEvre()">RSD->EUR</button>
        </div>
    `,
    data:function(){
        return {
            dinari: 0,
            evri: 0,
        }
    },
    methods:{
        pretvoriUDinare:function (){
            this.dinari = this.kursEvra*parseFloat(this.evri);
        },
        pretvoriUEvre:function(){
            this.evri = parseFloat(this.dinari)/this.kursEvra;
        }
    }
}