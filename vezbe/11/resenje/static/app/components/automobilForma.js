export default {
    props: ["automobil", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            noviAutomobil: this.automobil ? {...this.automobil} : {}
        }
    },
    watch: {
        automobil: function(newValue, oldValue) {
             this.noviAutomobil = {...this.automobil};
         }
    },
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...noviAutomobil})">
        <div>
            <label>Registarski broj: <input type="text" v-model="noviAutomobil.registarski_broj" required minLength="6" maxLength="6"></label>
        </div>
        <div>
            <label>Marka: <input type="text" v-model="noviAutomobil.marka" required></label>
        </div>
        <div>
            <label>Model: <input type="text" v-model="noviAutomobil.model" required></label>
        </div>
        <div>
            <label>Zapremina motora: <input type="number" v-model="noviAutomobil.zapremina_motora" required></label>
        </div>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}