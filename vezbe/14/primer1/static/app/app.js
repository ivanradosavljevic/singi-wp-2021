import Prodavnica from './components/prodavnica.js'
import TabelaKorisnika from './components/tabelaKorisnika.js'
import KorisnikForma from './components/korisnikForma.js'
import TabelaProizvoda from './components/tabelaProizvoda.js';
import Kupci from './components/kupci.js'
import Proizvodi from './components/proizvodi.js'
import Proizvod from './components/proizvod.js'
import Login from './components/login.js'
import Logout from './components/logout.js'

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes: [
        {path: "/", component: Kupci},
        {path: "/login", component: Login},
        {path: "/logout", component: Logout},
        {path: "/proizvodi", component: Proizvodi},
        {path: "/proizvodi/:id", component: Proizvod}
    ], 
});

const app = Vue.createApp(Prodavnica);
app.component('tabela-korisnika', TabelaKorisnika);
app.component('tabela-proizvoda', TabelaProizvoda);
app.component('korisnik-forma', KorisnikForma);
app.use(router);
app.mount("#app");