import flask
from flask import Flask
import json 

import view
import service
import repository

app = Flask(__name__)


@app.route("/studenti")
def greet():
    studenti = repository.pripremi_listu_studenata()
    studenti = service.servis(studenti)
    ret = view.generisi_html_3(studenti)
    return ret

# @app.route("/profesori")
# def profesori():
#     profesori = repository.pripremi_listu_profesora()
#     profesori = service.servis(profesori)
#     ret = view.generisi_html_3(profesori)
#     return ret


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)