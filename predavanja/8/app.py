from flask import Flask, render_template, request, redirect
import pymysql.cursors
import json

import users

app = Flask(__name__, static_folder="static", static_url_path="/")

conn = pymysql.connect( host='localhost',
                        user='root',
                        password='levo.desno',
                        database='primer1',
                        charset='utf8mb4',
                        cursorclass=pymysql.cursors.DictCursor)

@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route('/<path:path>')
def send_file(path):
    return app.send_static_file(path)

users.init(app)

# @app.route("/users")
# def index():
#     users = [
#         'petar',
#         'milica',
#         'marko'
#     ]
#     return json.dumps(users)
#     # with conn.cursor() as cursor:
#     #     cursor.execute("SELECT * FROM users")
#     #     for el in cursor.fetchall():
#     #         el['spojeno'] = el['email']+'====='+el['password']
#     #         users.append(el)
#     # return render_template('index.html', users=users)

@app.route("/add_user")
def add_user():
    return render_template('add_user.html')


@app.route("/add", methods=['POST'])
def add():
    obj = request.form
    with conn.cursor() as cursor:
        sql = "INSERT INTO users (email, password) VALUES (%s, %s)"
        cursor.execute(sql, (obj['email'], obj['password']))
    conn.commit()
    return redirect('/')

@app.route("/trazi", methods=['POST'])
def trazi():
    obj = request.form
    users = []
    with conn.cursor() as cursor:
        sql = "SELECT * FROM users WHERE email LIKE %s"
        cursor.execute(sql, ('%'+obj['email']+'%', ))
        for el in cursor.fetchall():
            el['spojeno'] = el['email']+'====='+el['password']
            users.append(el)
    return render_template('index.html', users=users)


@app.route("/edit_user", methods=['GET'])
def edit_user():
    obj = request.args
    cursor = conn.cursor()
    sql = "SELECT * FROM users WHERE id=%s"
    cursor.execute(sql, (obj['id'],))
    user = cursor.fetchone()
    cursor.close()
    return render_template('edit_user.html', user=user)

@app.route("/edit_user_action", methods=['POST'])
def edit_user_action():
    args = request.args
    obj = request.form
    print(obj)
    with conn.cursor() as cursor:
        sql = "UPDATE users SET email=%s, password=%s WHERE id=%s"
        cursor.execute(sql, (obj['email'], obj['password'], obj['id']))
    conn.commit()
    return redirect('/')

@app.route("/delete", methods=['GET'])
def delete():
    obj = request.args
    print(obj)
    cursor = conn.cursor()
    sql = "DELETE FROM users WHERE id=%s"
    cursor.execute(sql, (  obj['id'], ) )
    cursor.close()
    conn.commit()
    return redirect('/')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, )