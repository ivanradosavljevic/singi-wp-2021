import json
from flask import request

conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/api/student', view_func=student_create, methods=['POST'])
    app.add_url_rule('/api/student', view_func=student_read, methods=['GET'])
    app.add_url_rule('/api/student/<int:id>', view_func=student_update, methods=['PUT'])
    app.add_url_rule('/api/student/<int:id>', view_func=student_delete, methods=['DELETE'])

def student_create():
    with conn.cursor() as cursor:
        cursor.execute("INSERT INTO student(ime, prezime, indeks) VALUES (%(ime)s, %(prezime)s, %(indeks)s)", request.json)
    conn.commit()
    return 'ok', 201

def student_read():
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT id, ime, prezime, indeks FROM student")
        for el in cursor.fetchall():
            ret.append(el)
    return json.dumps(ret), 200

def student_update(id):
    student = dict(request.json)
    student['id'] = id
    with conn.cursor() as cursor:
        cursor.execute("UPDATE student SET ime = %(ime)s, prezime = %(prezime)s, indeks = %(indeks)s WHERE id = %(id)s", student)
    conn.commit()
    return 'ok', 201

def student_delete(id):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM student WHERE id = %s", (id,))
    conn.commit()
    return 'ok', 201

