export default {
    template: `<button v-on:click="logOut()">Log out</button>`,
    methods: {
        logOut() {
            axios.get(`api/logout`).then((response) => {
            }, _ => {
                this.neuspesanLogin = true;
            });
        }
    }
}