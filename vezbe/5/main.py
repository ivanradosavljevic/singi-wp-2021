import flask
from flask import Flask
from flaskext.mysql import MySQL
from flaskext.mysql import pymysql

app = Flask(__name__, static_url_path="/")
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "prodavnica"

mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

@app.route("/")
def home():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac")
    kupci = cursor.fetchall()
    return flask.render_template("kupci.tpl.html", kupci=kupci)

@app.route("/dodajKupca", methods=["POST"])
def dodavanje_kupca():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupac(korisnickoIme, lozinka, ime, prezime) VALUES(%(korisnickoIme)s, %(lozinka)s, %(ime)s, %(prezime)s)", flask.request.form)
    db.commit()
    return flask.redirect("/")

@app.route("/kupacIzmena", methods=["GET"])
def kupac_izmena():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac WHERE id=%s", (flask.request.args["id"], ))
    kupac = cursor.fetchone()
    return flask.render_template("kupacIzmena.tpl.html", kupac=kupac)

@app.route("/izmeniKupca", methods=["POST"])
def izmeni_kupca():
    kupac = dict(flask.request.form)
    kupac["id"] = flask.request.args["id"]
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE kupac SET korisnickoIme=%(korisnickoIme)s, lozinka=%(lozinka)s, ime=%(ime)s, prezime=%(prezime)s WHERE id=%(id)s", korisnik)
    db.commit()
    return flask.redirect("/")

@app.route("/ukloniKupca", methods=["GET"])
def ukloni_kupca():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM kupac WHERE id=%s", (flask.request.args["id"], ))
    db.commit()
    return flask.redirect("/")


@app.route("/proizvodi")
def proizvodi():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod")
    proizvodi = cursor.fetchall()
    return flask.render_template("proizvodi.tpl.html", proizvodi=proizvodi)

@app.route("/dodajProizvod", methods=["POST"])
def dodavanje_proizvoda():
    proizvod = dict(flask.request.form)
    proizvod["dostupno"] = proizvod.get("dostupno", 0)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO proizvod(naziv, opis, cena, dostupno) VALUES(%(naziv)s, %(opis)s, %(cena)s, %(dostupno)s)", proizvod)
    db.commit()
    return flask.redirect("/proizvodi")

@app.route("/proizvodIzmena", methods=["GET"])
def proizvod_izmena():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod WHERE id=%s", (flask.request.args["id"], ))
    proizvod = cursor.fetchone()
    return flask.render_template("proizvodIzmena.tpl.html", proizvod=proizvod)

@app.route("/izmeniProizvod", methods=["POST"])
def izmeni_proizvod():
    proizvod = dict(flask.request.form)
    proizvod["id"] = flask.request.args["id"]
    proizvod["dostupno"] = proizvod.get("dostupno", 0)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE proizvod SET naziv=%(naziv)s, cena=%(cena)s, opis=%(opis)s, dostupno=%(dostupno)s WHERE id=%(id)s", proizvod)
    db.commit()
    return flask.redirect("/proizvodi")

@app.route("/ukloniProizvod", methods=["GET"])
def ukloni_proizvod():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM proizvod WHERE id=%s", (flask.request.args["id"], ))
    db.commit()
    return flask.redirect("/proizvodi")

@app.route("/kupovine")
def kupovine():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupovina")
    kupovine = cursor.fetchall()
    return flask.render_template("kupovine.tpl.html", kupovine=kupovine)

@app.route("/kupovinaDodavanje", methods=["GET"])
def kupovina_dodavanje():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac")
    kupci = cursor.fetchall()
    cursor.execute("SELECT * FROM proizvod WHERE dostupno=1")
    proizvodi = cursor.fetchall()
    return flask.render_template("kupovinaDodavanje.tpl.html", kupci=kupci, proizvodi=proizvodi)

@app.route("/dodajKupovinu", methods=["POST"])
def dodavanje_kupovine():
    kupovina = dict(flask.request.form)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupovina(kupac_id, proizvod_id, kolicina, cena, datumKupovine) VALUES(%(kupac_id)s, %(proizvod_id)s, %(kolicina)s, %(cena)s, %(datumKupovine)s)", kupovina)
    db.commit()
    return flask.redirect("/kupovine")

@app.route("/ukloniKupovinu", methods=["GET"])
def ukloni_kupovinu():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM kupovina WHERE id=%s", (flask.request.args["id"], ))
    db.commit()
    return flask.redirect("/kupovine")