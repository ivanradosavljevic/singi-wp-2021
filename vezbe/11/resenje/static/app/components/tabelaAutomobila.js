export default {
    props: ["automobili"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table>
<thead>
    <tr>
        <th>ID</th>
        <th>Registarski broj</th>
        <th>Marka</th>
        <th>Model ime</th>
        <th>Zapremina motora</th>
    </tr>
</thead>
<tbody>
    <tr v-for="automobil in automobili">
        <td>{{automobil.id}}</td>
        <td>{{automobil.registarski_broj}}</td>
        <td>{{automobil.marka}}</td>
        <td>{{automobil.model}}</td>
        <td>{{automobil.zapremina_motora}}</td>
        <td><button v-on:click="$emit('izmena', {...automobil})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', {...automobil})">Ukloni</button></td>
    </tr>
</tbody>
</table>
    `
}