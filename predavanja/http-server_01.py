import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 8080)
print("server pokrenut")
sock.bind(server_address)

sock.listen(0)

while True:
    print("waiting for a connection")
    connection, client_address = sock.accept()
    print("connection from {}".format(client_address))
    data = connection.recv(1024)
    print('|',data, '|')

    connection.sendall(b"HTTP/1.0 200 OK\r\n")
    connection.sendall(b"Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n")
    connection.sendall(b"Server: Apache/0.8.4\r\n")
    connection.sendall(b"Content-Type: text/html\r\n")
    connection.sendall(b"Content-Length: 137\r\n")
    connection.sendall(b"Expires: Sat, 01 Jan 2000 00:59:59 GMT\r\n")
    connection.sendall(b"Last-modified: Fri, 09 Aug 1996 14:21:40 GMT\r\n")
    connection.sendall(b"Connection: close\r\n")
    connection.sendall(b"\r\n")
    connection.sendall(b"<TITLE>Example</TITLE>")
    connection.sendall(b"<html><body><P>Primer 5</P>")
    for i in range(10):
        print('------------')
        connection.sendall(b"-----<br>")
        connection.sendall(("--->{}<br>".format(i)).encode())
    connection.sendall(b"</body></html>")
    connection.close()
    print('-----------')