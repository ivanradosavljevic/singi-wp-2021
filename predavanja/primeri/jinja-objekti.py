from jinja2 import Template

class Person:

    def __init__(self, name, age):

        self.name = name
        self.age = age

person = Person('Peter', 34)

tm = Template("My name is {{ per.name }} and I am {{ per.age }}")
msg = tm.render(per=person)

print(msg)