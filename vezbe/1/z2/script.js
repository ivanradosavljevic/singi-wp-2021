function validacija() {
    const regex = new RegExp("^[0-9]{4}/[0-9]{6}$");
    const indeks = document.querySelector("#indeks").value;
    const rezultat = regex.test(indeks);

    if(rezultat) {
        return true;
    }
    window.alert(rezultat);
    return false;
}