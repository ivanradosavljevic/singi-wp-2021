export default {
    props: ["osiguravajucaKuca", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            novaOsiguravajucaKuca: this.osiguravajucaKuca ? {...this.osiguravajucaKuca} : {}
        }
    },
    watch: {
        osiguravajucaKuca: function(newValue, oldValue) {
             this.novaOsiguravajucaKuca = {...this.osiguravajucaKuca};
         }
    },
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...novaOsiguravajucaKuca})">
        <div>
            <label>Naziv: <input type="text" v-model="novaOsiguravajucaKuca.naziv" required></label>
        </div>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}