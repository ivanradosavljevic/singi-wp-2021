#!/usr/bin/env python3

from flask import Flask, render_template, request
app = Flask(__name__)

@app.route("/primer1")
def greet():
    username = request.args.get('name')
    return render_template('primer1.html', name=username)

@app.route("/saberi")
def sabiranje():
    a = request.args.get('a')
    b = request.args.get('b')
    c = float(a) + float(b)
    return f"Zbir a+b = {c}"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)