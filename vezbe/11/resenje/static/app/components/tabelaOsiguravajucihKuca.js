export default {
    props: ["osiguravajuceKuce"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    template: `
<table>
<thead>
    <tr>
        <th>ID</th>
        <th>Naziv</th>
    </tr>
</thead>
<tbody>
    <tr v-for="osiguravajucaKuca in osiguravajuceKuce">
        <td>{{osiguravajucaKuca.id}}</td>
        <td>{{osiguravajucaKuca.naziv}}</td>
        <td><button v-on:click="$emit('izmena', {...osiguravajucaKuca})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', {...osiguravajucaKuca})">Ukloni</button></td>
    </tr>
</tbody>
</table>
    `
}