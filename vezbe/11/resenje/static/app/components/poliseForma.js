export default {
    props: ["polisa", "automobili", "osiguravajuceKuce", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            novaPolisa: this.polisa ? {...this.polisa} : {}
        }
    },
    watch: {
        polisa: function(newValue, oldValue) {
             this.novaPolisa = {...this.polisa};
         }
    },
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...novaPolisa})">
        <div>
            <label>Automobil:
                <select v-model="novaPolisa.automobil_id" required>
                    <option v-for="automobil in automobili" v-bind:value="automobil.id">{{automobil.registarski_broj}} | {{automobil.marka}} | {{automobil.model}}</option>
                </select>
            </label>
        </div>
        <div>
            <label>Osiguravajuća kuća:
                <select v-model="novaPolisa.osiguravajuca_kuca_id" required>
                    <option v-for="osiguravajucаKucа in osiguravajuceKuce" v-bind:value="osiguravajucаKucа.id">{{osiguravajucаKucа.id}} | {{osiguravajucаKucа.naziv}}</option>
                </select>
            </label>
        </div>
        <div>
            <label>Datum početka: <input type="datetime-local" v-model="novaPolisa.datum_pocetka" required></label>
        </div>
        <div>
            <label>Datum kraja: <input type="datetime-local" v-model="novaPolisa.datum_kraja" required></label>
        </div>
        <div>
            <label>Cena: <input type="number" v-model="novaPolisa.cena" required></label>
        </div>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}