export var AppPolisaComponent = {

    template:`
    <div class="polisa">
        <h1> Polisa </h1>
        <button v-on:click="create()">Create</button>
        <button v-on:click="update()">Update</button><br>
        <form>
        automobil_id <input v-model="izabrana_polisa.automobil_id"><br>
        osiguravajuca_kuca_id

        <select v-model="izabrana_polisa.osiguravajuca_kuca_id">
            <option v-for="el in osiguravajuce_kuce" :value="el.id">{{el.naziv}}</option>
        </select><br>

        datum_pocetka <input type="datetime-local" v-model="izabrana_polisa.datum_pocetka"><br>
        datum_kraja <input type="datetime-local" v-model="izabrana_polisa.datum_kraja"><br>
        cena  <input v-model="izabrana_polisa.cena"><br>

        </form>
        <div v-for="polisa in polise">
            <a href="#" v-on:click="select(polisa)">{{polisa.datum_pocetka}}</a>
            {{polisa.datum_kraja}} {{polisa.cena}} 
            <a href="#" v-on:click="delete_polisa(polisa.id)">delete</a>
        </div>

    </div>
    `,
    data:function(){
        return {
            polise:[],
            osiguravajuce_kuce:[],
            izabrana_polisa:{
                naziv:''
            }
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(polisa){
            this.izabrana_polisa = {...polisa}
        },
        create:function(){
            axios.post("/api/polisa", this.izabrana_polisa).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function(){
            axios.get("/api/polisa").then(
                response =>{
                    this.polise = response.data;
                    this.izabrana_polisa = {...this.polise[0]};
                } 
            );
            axios.get("/api/osiguravajuca_kuca").then(
                response =>{
                    this.osiguravajuce_kuce = response.data;
                } 
            )
        },
        update:function(){
            axios.put("/api/polisa/"+this.izabrana_polisa.id, this.izabrana_polisa).then(
                response =>{
                    this.read();
                } 
            )
        },
        delete_polisa:function(id){
            axios.delete("/api/polisa/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    }

}