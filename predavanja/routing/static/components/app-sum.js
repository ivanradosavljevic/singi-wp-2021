export var AppSumComponent = {
    props:{
        naslov:String
    },
    template:`
        <div class="calculator">
            <h2> Kalkulator </h2>
            A = <input v-model="a"><br>
            B = <input v-model="b"><br>
            <button v-on:click="saberi()">Saberi</button>
            C = {{c}}<br>
            Naslov od parent komponente: {{naslov}}

        </div>
    `,
    data:function(){
        return {
            a: 0,
            b: 0,
            c: 0
        }
    },
    methods:{
        saberi:function (){
            this.c = parseFloat(this.a) + parseFloat(this.b);
        }
    }
};