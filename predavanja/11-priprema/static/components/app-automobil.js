export var AppAutomobilComponent = {

    template:`
    <div class="automobil">
        <h1> Automobil </h1>
        <button v-on:click="create()">Create</button>
        <button v-on:click="update()">Update</button><br>
        <form>
        registarski_broj <input v-model="izabran_automobil.registarski_broj"><br>
        marka <input v-model="izabran_automobil.marka"><br>
        model <input v-model="izabran_automobil.model"><br>
        zapremina_motora  <input v-model="izabran_automobil.zapremina_motora"><br>

        </form>
        <div v-for="automobil in automobili">
            {{automobil.id}} <a href="#" v-on:click="select(automobil)">{{automobil.registarski_broj}}</a>
            {{automobil.marka}} {{automobil.model}} {{automobil.zapremina_motora}}
            <a href="#" v-on:click="delete_automobil(automobil.id)">delete</a>
        </div>

        <!--app-mesto v-bind:automobil="izabran_automobil"></app-mesto-->

    </div>
    `,
    data:function(){
        return {
            automobili:[],
            izabran_automobil:{
                naziv:''
            }
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(automobil){
            this.izabran_automobil = {...automobil}
        },
        create:function(){
            axios.post("/api/automobil", this.izabran_automobil).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function(){
            axios.get("/api/automobil").then(
                response =>{
                    this.automobili = response.data;
                    this.izabran_automobil = {...this.automobili[0]};
                } 
            )
        },
        update:function(){
            axios.put("/api/automobil/"+this.izabran_automobil.id, this.izabran_automobil).then(
                response =>{
                    this.read();
                } 
            )
        },
        delete_automobil:function(id){
            axios.delete("/api/automobil/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    }

}