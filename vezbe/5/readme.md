# Vežbe 5
*Cilj vežbi: Pripreme za kolokvijum.*

## Zadaci
1. Upotrebom Flask radnog okvira i Jinja2 šablona napraviti web aplikaciju za upravljanje entitetima datim u dijagramu. Za svaki entitet omogućiti tabelarni prikaz, prikaz detalja, dodavanje, brisanje i izmenu. Ispoštovati ograničenja navedena u dijagramu.
2. Proširiti aplikaciju tako da je prilikom dodavanja ili brisanja entiteta koji sadrže referencu na druge entitet izbor povezanih entiteta moguće vršiti preko select polja.
3. Dodati mogućnost pretrage proizvoda po svim atributima, pri čemu je pretragu po ceni moguće vršiti za zadati opseg.
4. Onemogućiti kreiranje kupovine za proizvode kojih nema na stanju.
5. U tabelarnom prikazu proizvoda, proizvode kojih nema na stanju izdvojiti u odvojenu tabelu.
6. U formi za dodavanje i izmenu korisnika dodati validaciju na strani klijenta. Prilikom neuspešne validacije za svako polje koje nije ispravno popunjeno ispisati tekst sa uputstvima za ispravno popunjavanje polja.
___
### Dodatne napomene:
* Dokumentacija za Flask radni okvir: https://palletsprojects.com/p/flask/.
* Dokumentacija za Jinja2 šablone: https://palletsprojects.com/p/jinja/
___