import json

conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/mesta/<int:id>', view_func=read, methods=['GET'])

def read(id):
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT * FROM mesto WHERE id_drzava=%s", (id, ))
        for el in cursor.fetchall():
            ret.append(el)
    return json.dumps(ret)


