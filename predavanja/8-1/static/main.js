import { AppSumComponent } from "/components/app-sum.js";
import { AppExchangeComponent} from "/components/app-exchange.js";
import { AppUsers } from "/components/app-users.js";


Vue.component('app-sum', AppSumComponent);
Vue.component('app-exchange', AppExchangeComponent);
Vue.component('app-users', AppUsers);

var app = new Vue({
    el: "#app",
    template:`
        <div>
            Danas je {{title}}<br>
            <app-sum v-bind:naslov="title"></app-sum>
            <!-- <app-exchange kursEvra=118.5></app-exchange> -->
            <app-users></app-users>
        </div>
    `,
    data:{
        title:"sreda 21-04-2021"
    }
})