export default {
    props: ["kupac", "tekst"],
    emits: ["sacuvaj"],
    data() {
        return {
            noviKupac: this.kupac ? {...this.kupac} : {}
        }
    },
    watch: {
         kupac: function(newValue, oldValue) {
             this.noviKupac = {...this.kupac};
         }
    },
    template: `
    <form v-on:submit.prevent="$emit('sacuvaj', {...noviKupac})">
        <div>
            <label>Ime: <input type="text" v-model="noviKupac.ime" required></label>
        </div>
        <div>
            <label>Prezime: <input type="text" v-model="noviKupac.prezime" required></label>
        </div>
        <div>
            <label>Korisničko ime: <input type="text" v-model="noviKupac.korisnickoIme" required></label>
        </div>
        <div>
            <label>Lozinka: <input type="password" v-model="noviKupac.lozinka" required></label>
        </div>
        <div>
            <input type="submit" v-bind:value="tekst">
        </div>
    </form>
    `
}