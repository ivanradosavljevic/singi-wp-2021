export default {
    template: `
    <div class="alert alert-danger" role="alert" v-if="neuspesanLogin">
  Neuspesna prijava na sistem!
</div>
    <form v-on:submit.prevent="login()">
  <div class="mb-3">
    <label class="form-label">Korisničko ime</label>
    <input type="text" v-model="korisnik.korisnicko_ime" class="form-control" required>
  </div>
  <div class="mb-3">
    <label class="form-label">Lozinka</label>
    <input type="password" v-model="korisnik.lozinka" class="form-control" required>
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
</form>
    `,
    data: function() {
        return {
            korisnik: {
                "korisnicko_ime": "",
                "lozinka": ""
            },
            neuspesanLogin: false
        };
    },
    methods: {
        login: function() {
            // console.log(this.korisnik);
            axios.post(`api/login`, this.korisnik).then((response) => {
                this.$router.push("/");
            }, _ => {
                this.neuspesanLogin = true;
            });
        }
    }
}