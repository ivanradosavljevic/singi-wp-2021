export var AppOsiguravajucaKucaComponent = {

    template:`
    <div class="osiguravajuca_kuca">
        <h1> Osiguravajuce kuce </h1>
        <button v-on:click="create()">Create</button>
        <button v-on:click="update()">Update</button><br>
        <form>
            <input v-model="izabrana_osiguravajuca_kuca.naziv">
        </form>
        <div v-for="osiguravajuca_kuca in osiguravajuca_kuce">
            {{osiguravajuca_kuca.id}} <a href="#" v-on:click="select(osiguravajuca_kuca)">{{osiguravajuca_kuca.naziv}}</a>
            <a href="#" v-on:click="delete_osiguravajuca_kuca(osiguravajuca_kuca.id)">delete</a>
        </div>

    </div>
    `,
    data:function(){
        return {
            osiguravajuca_kuce:[],
            izabrana_osiguravajuca_kuca:{
                naziv:''
            }
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(osiguravajuca_kuca){
            this.izabrana_osiguravajuca_kuca = {...osiguravajuca_kuca}
        },
        create:function(){
            axios.post("/api/osiguravajuca_kuca", this.izabrana_osiguravajuca_kuca).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function(){
            axios.get("/api/osiguravajuca_kuca").then(
                response =>{
                    this.osiguravajuca_kuce = response.data;
                    this.izabrana_osiguravajuca_kuca = {...this.osiguravajuca_kuce[0]};
                } 
            )
        },
        update:function(){
            axios.put("/api/osiguravajuca_kuca/"+this.izabrana_osiguravajuca_kuca.id, this.izabrana_osiguravajuca_kuca).then(
                response =>{
                    this.read();
                } 
            )
        },
        delete_osiguravajuca_kuca:function(id){
            axios.delete("/api/osiguravajuca_kuca/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    }

}