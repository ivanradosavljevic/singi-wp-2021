import flask
from flask import Blueprint

from flask_jwt_extended import jwt_required

from utils.db import mysql

kupci_blueprint = Blueprint("kupci_blueprint", __name__)

@kupci_blueprint.route("")
@jwt_required()
def get_all_kupci():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac")
    kupci = cursor.fetchall()
    return flask.jsonify(kupci)

@kupci_blueprint.route("<int:kupac_id>")
def get_kupac(kupac_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac WHERE id=%s", (kupac_id,))
    kupac = cursor.fetchone()
    if kupac is not None:
        return flask.jsonify(kupac)
    
    return "", 404

@kupci_blueprint.route("", methods=["POST"])
def dodavanje_kupca():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupac(korisnickoIme, lozinka, ime, prezime) VALUES(%(korisnickoIme)s, %(lozinka)s, %(ime)s, %(prezime)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@kupci_blueprint.route("<int:kupac_id>", methods=["PUT"])
def izmeni_kupca(kupac_id):
    kupac = dict(flask.request.json)
    kupac["kupac_id"] = kupac_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE kupac SET korisnickoIme=%(korisnickoIme)s, lozinka=%(lozinka)s, ime=%(ime)s, prezime=%(prezime)s WHERE id=%(kupac_id)s", kupac)
    db.commit()
    cursor.execute("SELECT * FROM kupac WHERE id=%s", (kupac_id,))
    kupac = cursor.fetchone()
    return flask.jsonify(kupac)

@kupci_blueprint.route("<int:kupac_id>", methods=["DELETE"])
def ukloni_kupca(kupac_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM kupac WHERE id=%s", (kupac_id, ))
    db.commit()
    return ""
