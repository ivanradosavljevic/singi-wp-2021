from flask import Flask, render_template, request, redirect
import pymysql.cursors
import json

app = Flask(__name__, static_folder="static", static_url_path="/")

conn = pymysql.connect( host='localhost',
                        user='root',
                        password='levo.desno',
                        database='primer1',
                        charset='utf8mb4',
                        cursorclass=pymysql.cursors.DictCursor)


@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route('/<path:path>')  # /components/app-users.js
def send_file(path):
    return app.send_static_file(path)


@app.route("/users")
def users():
    # return json.dumps(['sofija', 'milica', 'marija'])
    users = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT email FROM users")
        for el in cursor.fetchall():
            users.append(el)
    return json.dumps(users)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, )