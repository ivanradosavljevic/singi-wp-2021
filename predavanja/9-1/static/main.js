import { AppDrzavaComponent } from "/components/app-drzava.js";
import { AppMestoComponent } from "/components/app-mesto.js";


Vue.component('app-drzava', AppDrzavaComponent);
Vue.component('app-mesto', AppMestoComponent);


var app = new Vue({
    el: "#app",
    template:`
        <div>
            <h1>Nedelja 9 {{title}}</h1>
            <app-drzava></app-drzava>
            
        </div>
    `,
    data:{
        title:"sreda 28-04-2021"
    }
})