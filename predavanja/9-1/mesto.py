import json
from flask import request

conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/api/mesto', view_func=mesto_create, methods=['POST'])
    app.add_url_rule('/api/mesto/<int:id_drzava>', view_func=mesto_read, methods=['GET'])
    app.add_url_rule('/api/mesto/<int:id>', view_func=mesto_update, methods=['PUT'])
    app.add_url_rule('/api/mesto/<int:id>', view_func=mesto_delete, methods=['DELETE'])

def mesto_create():
    with conn.cursor() as cursor:
        cursor.execute("INSERT INTO mesto(naziv, id_drzava) VALUES (%(naziv)s, %(id_drzava)s)", request.json)
    conn.commit()
    return 'ok', 201

def mesto_read(id_drzava):
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT id, naziv, id_drzava FROM mesto WHERE id_drzava=%s", (id_drzava,))
        for el in cursor.fetchall():
            ret.append(el)
    return json.dumps(ret), 200

def mesto_update(id):
    mesto = dict(request.json)
    mesto['id'] = id
    with conn.cursor() as cursor:
        cursor.execute("UPDATE mesto SET naziv = %(naziv)s, id_drzava= %(id_drzava)s WHERE id = %(id)s", mesto)
    conn.commit()
    return 'ok', 201

def mesto_delete(id):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM mesto WHERE id = %s", (id,))
    conn.commit()
    return 'ok', 201

