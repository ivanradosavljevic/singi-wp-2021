export default {
    data() {
        return {
            kupci: [],
            proizvodi: [],
            kupacZaIzmenu: {},
            stranicaZaPrikaz: "kupci"
        }
    },
    methods: {
        setKupacZaIzmenu(kupac) {
            this.kupacZaIzmenu = { ...kupac };
        },
        refreshKupci() {
            axios.get("api/kupci").then((response) => {
                this.kupci = response.data;
            });
        },
        refreshProizvodi() {
            axios.get("api/proizvodi").then((response) => {
                this.proizvodi = response.data;
            });
        },
        create(kupac) {
            axios.post("api/kupci", kupac).then((response) => {
                this.refreshKupci();
            });
        },
        update(kupac) {
            console.log(kupac);
            axios.put(`api/kupci/${kupac.id}`, kupac).then((response) => {
                this.refreshKupci();
            });
        },
        remove(id) {
            axios.delete(`api/kupci/${id}`).then((response) => {
                this.refreshKupci();
            });
        },
        removeProizvod(id) {
            axios.delete(`api/proizvodi/${id}`).then((response) => {
                this.refreshProizvodi();
            });
        },
        navigate(page) {
            this.stranicaZaPrikaz = page;
        }
    },
    created() {
        this.refreshKupci();
        this.refreshProizvodi();
    }
}