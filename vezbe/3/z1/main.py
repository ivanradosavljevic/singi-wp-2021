import flask
from flask import Flask

app = Flask(__name__, static_folder="static", static_url_path="/")

studenti = [{"indeks": "1234/123456", "ime": "Ime", "prezime": "Prezime", "ocena": 10, "obrisan": False}]

@app.route("/")
def home():
    return flask.render_template("index.tpl.html", studenti=studenti)

@app.route("/dodajStudenta", methods=["POST"])
def dodaj_studenta():
    student = dict(flask.request.form)
    student["obrisan"] = False
    studenti.append(student)
    return flask.redirect("/")

@app.route("/student", methods=["GET"])
def dobavi_studenta():
    indeks = flask.request.args["indeks"]
    for s in studenti:
        if s["indeks"] == indeks:
            return flask.render_template("student.tpl.html", **s)
    return app.send_static_file("notFound.html")

@app.route("/ukloniStudenta", methods=["GET"])
def ukloni_studenta():
    for i, s in enumerate(studenti):
        if s["indeks"] == flask.request.args["indeks"]:
            studenti[i]["obrisan"] = not studenti[i]["obrisan"]
    return flask.redirect("/")

@app.route("/izmeniStudentaForma", methods=["GET"])
def izmeni_studenta_forma():
    for i, s in enumerate(studenti):
        if s["indeks"] == flask.request.args["indeks"]:
            return flask.render_template("izmeniStudenta.tpl.html", **s)
    return flask.redirect("notFound.html")

@app.route("/izmeniStudenta", methods=["POST"])
def izmeni_studenta():
    for i, s in enumerate(studenti):
        if s["indeks"] == flask.request.args["indeks"]:
            student = dict(flask.request.form)
            student["obrisan"] = studenti[i]["obrisan"]
            studenti[i] = student
    return flask.redirect("/")

if __name__ == "__main__":
    app.run()