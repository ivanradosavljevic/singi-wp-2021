#!/usr/bin/env python3

from jinja2 import Template

obj = {'name': 'pera'}

template = "Hello {{ obj.name }}"

tm = Template(template)
msg = tm.render(obj=obj)

print(msg)