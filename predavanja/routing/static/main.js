Vue.use(VueRouter)

const Foo = { template: '<div>foo</div>',
    computed: {
        username(){
            return global.user;
        }
    }
}
const Bar = { template: '<div>bar</div>' };

const Users = {
    template: `
        <div>    
            <h1>Users</h1>
            <router-link v-for="user in users" :key="user.id" :to="user.link()">{{user.name}}</router-link>
        </div>
    `,
    data:function(){
        return {
            users:[
                {id:1, name:'pera', link:function(){return "/user/"+this.id+"/profile"}},
                {id:2, name:'mika', link:function(){return "/user/"+this.id+"/profile"}
            ]
        }
    }
};


const User = {
    template: `
        <div>    
            <h1>User {{$route.params.id}}</h1>
            <router-link to="/user/1/profile">Profile</router-link>
            <router-link to="posts">Posts</router-link>
        </div>
    `
}

const UserPosts = {
    template: `
        <div>    
            <h1>UserPosts</h1>
        </div>
    `
};


const routes = [
  { path: '/foo', component: Foo },
  { path: '/bar', component: Bar },
  { path: '/users', component: Users},
  { 
      path: '/user/:id', 
      component: User, 
      children:[
              {path: 'posts', component:UserPosts}
          ]
      }
]

const router = new VueRouter({
  routes
})

const app = new Vue({
  el:"#app",
  router
})