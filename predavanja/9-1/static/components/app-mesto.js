export var AppMestoComponent = {

    template:`
    <div class="mesto">
        <h1> Mesto </h1>
        <button v-on:click="create()">Create</button>
        <button v-on:click="update()">Update</button><br>
        <form>
            <input v-model="izabrana_mesto.naziv">
        </form>
        <div v-for="mesto in mesta">
            <a href="#" v-on:click="select(mesto)">{{mesto.naziv}}</a>
            <a href="#" v-on:click="delete_mesto(mesto.id)">delete</a>
        </div>
    </div>
    `,
    props:['drzava'],
    data:function(){
        return {
            mesta:[],
            izabrana_mesto:{
                naziv:''
            }
        }
    },
    watch:{
        drzava:function(newVal, oldVal){
            this.read();
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(mesto){
            this.izabrana_mesto = {...mesto}
        },
        create:function(){
            this.izabrana_mesto.id_drzava = this.drzava.id;
            axios.post("/api/mesto", this.izabrana_mesto).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function(){
            axios.get("/api/mesto/"+this.drzava.id).then(
                response =>{
                    this.mesta = response.data;
                } 
            )
        },
        update:function(){
            axios.put("/api/mesto/"+this.izabrana_mesto.id, this.izabrana_mesto).then(
                response =>{
                    this.read();
                } 
            )
        },
        delete_mesto:function(id){
            axios.delete("/api/mesto/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    }

}