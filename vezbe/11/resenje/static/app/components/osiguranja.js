export default {
    data() {
        return {
            automobili: [],
            polise: [],
            osiguravajuceKuce: [],
            automobilZaIzmenu: {},
            osiguravajucaKucaZaIzmenu: {},
            polisaZaIzmenu: {},
            stranicaZaPrikaz: "automobili"
        }
    },
    methods: {
        refreshAutomobili() {
            axios.get("api/automobili").then((response) => {
                this.automobili = response.data;
            });
        },
        setAutomobilZaIzmenu(automobil) {
            this.automobilZaIzmenu = { ...automobil };
        },
        createAutomobil(automobil) {
            axios.post("api/automobili", automobil).then((response) => {
                this.refreshAutomobili();
            });
        },
        updateAutomobil(automobil) {
            axios.put(`api/automobili/${automobil.id}`, automobil).then((response) => {
                this.refreshAutomobili();
            });
        },
        removeAutomobil(automobil) {
            axios.delete(`api/automobili/${automobil.id}`).then((response) => {
                this.refreshAutomobili();
            });
        },

        refreshOsiguravajuceKuce() {
            axios.get("api/osiguravajuceKuce").then((response) => {
                this.osiguravajuceKuce = response.data;
            });
        },
        setOsiguravajucaKucaZaIzmenu(osiguravajucaKuca) {
            this.osiguravajucaKucaZaIzmenu = { ...osiguravajucaKuca };
        },
        createOsiguravajucaKuca(osiguravajucaKuca) {
            axios.post("api/osiguravajuceKuce", osiguravajucaKuca).then((response) => {
                this.refreshOsiguravajuceKuce();
            });
        },
        updateOsiguravajucaKuca(osiguravajucaKuca) {
            axios.put(`api/osiguravajuceKuce/${osiguravajucaKuca.id}`, osiguravajucaKuca).then((response) => {
                this.refreshOsiguravajuceKuce();
            });
        },
        removeOsiguravajucaKuca(osiguravajucaKuca) {
            axios.delete(`api/osiguravajuceKuce/${osiguravajucaKuca.id}`).then((response) => {
                this.refreshOsiguravajuceKuce();
            });
        },

        refreshPolise() {
            axios.get("api/polise").then((response) => {
                for(let d of response.data) {
                    d.datum_pocetka = new Date(d.datum_pocetka).toISOString().split("Z")[0];
                    d.datum_kraja = new Date(d.datum_kraja).toISOString().split("Z")[0];
                }
                this.polise = response.data;
            });
        },
        setPolisaZaIzmenu(polisa) {
            this.polisaZaIzmenu = { ...polisa };
        },
        createPolisa(polisa) {
            axios.post("api/polise", polisa).then((response) => {
                this.refreshPolise();
            });
        },
        updatePolisa(polisa) {
            axios.put(`api/polise/${polisa.id}`, polisa).then((response) => {
                this.refreshPolise();
            });
        },
        removePolisa(polisa) {
            axios.delete(`api/polise/${polisa.id}`).then((response) => {
                this.refreshPolise();
            });
        },

        navigate(page) {
            this.stranicaZaPrikaz = page;
        }
    },
    created() {
        this.refreshAutomobili();
        this.refreshPolise();
        this.refreshOsiguravajuceKuce();
        // this.refreshProizvodi();
    }
}