import json

from jinja2 import Template

def zaglavlje():
    ret = """
    <html> <head> </head><body>
    """
    return ret

def kraj():
    return """
    </body></html>
    """

def generisi_html(studenti):
    ret = zaglavlje()
    ret += "<table>"
    for student in studenti:
        ret += "<tr><td>"+student['ime']+"</td><td>"+str(student['prosek'])+"</td></tr>"
    ret += "</table>"
    ret += kraj()
    return ret

def generisi_html_2(studenti):
    studenti = sorted(studenti, key= lambda x:x["prosek"])
    ret = zaglavlje()
    ret += "<table>"
    for student in studenti:
        ret += "<tr><td>"+str(student['prosek'])+"</td><td>"+student['ime']+"</td></tr>"
    ret += "</table>"
    ret += kraj()
    return ret

def generisi_html_3(studenti):
    template = """
<html><head></head>
    <body>
        <table>
        {% for student in obj -%}
            <tr><td>{{student.ime}}</td><td>{{student.prosek}}</tr>
        {% endfor %}
        </table>
    </body>
</html>
    """
    tm = Template(template)
    ret = tm.render(obj=studenti)
    return ret


def generisi_json(studenti):
    ret = json.dumps(studenti)
    return ret



def proba():
    print('name:', __name__)