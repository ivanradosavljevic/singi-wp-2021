import json
from flask import request
'''
  `id` INT NOT NULL,
  `registarski_broj` VARCHAR(6) NOT NULL,
  `marka` TEXT NOT NULL,
  `model` TEXT NOT NULL,
  `zapremina_motora` DECIMAL NOT NULL,
'''
conn = None

def init(app, _conn):
    global conn
    conn = _conn
    app.add_url_rule('/api/automobil', view_func=automobil_create, methods=['POST'])
    app.add_url_rule('/api/automobil', view_func=automobil_read, methods=['GET'])
    app.add_url_rule('/api/automobil/<int:id>', view_func=automobil_update, methods=['PUT'])
    app.add_url_rule('/api/automobil/<int:id>', view_func=automobil_delete, methods=['DELETE'])

def automobil_create():
    with conn.cursor() as cursor:
        cursor.execute("INSERT INTO automobil(registarski_broj, marka, model, zapremina_motora) VALUES (%(registarski_broj)s, %(marka)s, %(model)s, %(zapremina_motora)s)", request.json)
    conn.commit()
    return 'ok', 201

def automobil_read():
    ret = []
    with conn.cursor() as cursor:
        cursor.execute("SELECT id, registarski_broj, marka, model, zapremina_motora FROM automobil")
        for el in cursor.fetchall():
            el['zapremina_motora'] = float(el['zapremina_motora'])
            ret.append(el)
    return json.dumps(ret), 200

def automobil_update(id):
    automobil = dict(request.json)
    automobil['id'] = id
    with conn.cursor() as cursor:
        cursor.execute("UPDATE automobil SET registarski_broj = %(registarski_broj)s, marka = %(marka)s,model = %(model)s,zapremina_motora = %(zapremina_motora)s WHERE id = %(id)s", automobil)
    conn.commit()
    return 'ok', 201

def automobil_delete(id):
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM automobil WHERE id = %s", (id,))
    conn.commit()
    return 'ok', 201

