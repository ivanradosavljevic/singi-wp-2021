import socket
import sys

def parse_request(data):
    data = data.decode()
    lines = data.split('\n')
    for line in lines:
        print(line)
    print('----------------------')
    method, url, version = lines[0].split(' ')
    print('method:', method)
    print('url:', url)
    print('version', version)
    return method, url

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = ('localhost', 8080)
    print("server pokrenut")
    sock.bind(server_address)

    sock.listen(0)

    while True:
        print("waiting for a connection")
        connection, client_address = sock.accept()
        print("connection from {}".format(client_address))
        data = connection.recv(1024)
        method, url = parse_request(data)

        if url == '/nesto':
            contents = [b"<TITLE>Example</TITLE>", b"<html><body><P>Primer 1</P>"]
            for i in range(20):
                contents.append(('{}<br>'.format(i)).encode())

            contents.append(b'</body></html>')
            length = 0
            for line in contents:
                length += len(line)

            connection.sendall(b"HTTP/1.0 200 OK\r\n")
            connection.sendall(b"Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n")
            connection.sendall(b"Server: Apache/0.8.4\r\n")
            connection.sendall(b"Content-Type: text/html\r\n")
            connection.sendall(("Content-Length: {}\r\n".format(length)).encode())
            connection.sendall(b"Expires: Sat, 01 Jan 2000 00:59:59 GMT\r\n")
            connection.sendall(b"Last-modified: Fri, 09 Aug 1996 14:21:40 GMT\r\n")
            connection.sendall(b"Connection: close\r\n")
            connection.sendall(b"\r\n")
            for line in contents:
                connection.sendall(line)
        else:
            connection.sendall(b"HTTP/1.0 404 FAIL\r\n")

        connection.close()
        print('-----------')

main()