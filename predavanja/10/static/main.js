import { AppDrzavaComponent } from "/components/app-drzava.js";
import { AppMestoComponent } from "/components/app-mesto.js";
import {AppStudentComponent} from "/components/app-student.js";


Vue.component('app-drzava', AppDrzavaComponent);
Vue.component('app-mesto', AppMestoComponent);
Vue.component('app-student', AppStudentComponent);

Vue.use(BootstrapVue)


var app = new Vue({
    el: "#app",
    template:`
        <div>
            <div>
            <b-navbar toggleable="lg" type="dark" variant="info">
            <b-navbar-brand href="#">Studentska služba</b-navbar-brand>
        
            <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>
        
            <b-collapse id="nav-collapse" is-nav>
                <b-navbar-nav>
                <b-nav-item href="#">Link</b-nav-item>
                <b-nav-item href="#" disabled>Disabled</b-nav-item>
                </b-navbar-nav>
        
                <!-- Right aligned nav items -->
                <b-navbar-nav class="ml-auto">
                <b-nav-form>
                    <b-form-input size="sm" class="mr-sm-2" placeholder="Search"></b-form-input>
                    <b-button size="sm" class="my-2 my-sm-0" type="submit">Search</b-button>
                </b-nav-form>
        
                <b-nav-item-dropdown text="Lang" right>
                    <b-dropdown-item href="#">EN</b-dropdown-item>
                    <b-dropdown-item href="#">ES</b-dropdown-item>
                    <b-dropdown-item href="#">RU</b-dropdown-item>
                    <b-dropdown-item href="#">FA</b-dropdown-item>
                </b-nav-item-dropdown>
        
                <b-nav-item-dropdown right>
                    <!-- Using 'button-content' slot -->
                    <template #button-content>
                    <em>User</em>
                    </template>
                    <b-dropdown-item href="#">Profile</b-dropdown-item>
                    <b-dropdown-item href="#">Sign Out</b-dropdown-item>
                </b-nav-item-dropdown>
                </b-navbar-nav>
            </b-collapse>
            </b-navbar>
        </div>
            
            
            <div>
            <b-button v-b-toggle.sidebar-1>Toggle Sidebar</b-button>
            <b-sidebar id="sidebar-1" title="Studenti" shadow>
              <div class="px-3 py-2">


              <div>
                <b-nav vertical class="w-25">
                    <b-nav-item active>Active</b-nav-item>
                    <b-nav-item>Link</b-nav-item>
                    <b-nav-item>Another Link</b-nav-item>
                    <b-nav-item disabled>Disabled</b-nav-item>
                </b-nav>
                </div>



              </div>
            </b-sidebar>
          </div>
          <app-student></app-student>


        </div>
    `,
    data:{
        title:"sreda 05-05-2021"
    }
})