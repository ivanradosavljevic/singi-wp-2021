from flask import Flask, render_template, request, redirect
import pymysql.cursors

app = Flask(__name__, static_folder='static')
conn = pymysql.connect(
    host='localhost',
    user='root',
    password = 'levo.desno',
    database = 'web_programiranje',
    charset = 'utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

@app.route("/hello")
def hello():
    return 'ok'

def get_users():
    ret = []
    with conn.cursor() as cursor:
        sql = "SELECT id, email, password FROM users"
        cursor.execute(sql)
        for el in cursor.fetchall():
            ret.append(el)
    return ret

@app.route('/users', methods=['GET'])
def users():
    lista_korisnika = get_users()
    rez = '<html>'
    rez += '<body>'
    rez += '<table>'
    for el in lista_korisnika:
        rez += '<tr><td>[{}]</td><td>{}</td><td>{}</td></tr>'.format(el['id'], el['email'], el['password'])
    rez += '</table>'
    rez += '</body></html>'
    return rez

@app.route('/users_2', methods=['GET'])
def users2():
    lista_korisnika = get_users()
    return render_template('users.html', users=lista_korisnika)


def add_user(obj):
    with conn.cursor() as cursor:
        sql = "INSERT INTO users (email, password) VALUES (%s, %s)"
        cursor.execute(sql, (obj['email'], obj['password']))
    conn.commit()

@app.route('/add', methods=['POST'])
def add():
    obj = request.form
    add_user(obj)
    return redirect('/users_2')

@app.route('/remove', methods=['GET'])
def delete():
    id = request.args['id']
    with conn.cursor() as cursor:
        sql = "DELETE FROM users WHERE id = %s"
        cursor.execute(sql, (id,))
    conn.commit()
    return redirect('/users_2')

@app.route('/edit-user', methods=['GET'])
def edit():
    id = request.args['id']
    obj =  None
    with conn.cursor() as cursor:
        sql = "SELECT id, email, password FROM users WHERE id = %s"
        cursor.execute(sql, (id,))
        obj = cursor.fetchone()
    if obj is not None:
        return render_template('edit-user.html', user=obj)
    else:
        return redirect('/users_2')


@app.route('/<path:path>')
def send_file(path):
    return app.send_static_file(path)




app.run(host='0.0.0.0', port=8000)