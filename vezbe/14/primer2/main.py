import flask
from flask import Flask
from flask import session
from flask_jwt_extended.utils import get_jwt

from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required

from flask_jwt_extended import JWTManager

from utils.db import mysql

from blueprints.kupci_blueprint import kupci_blueprint
from blueprints.proizvodi_blueprint import proizvodi_blueprint

app = Flask(__name__, static_url_path="/")
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "prodavnica"
app.config["JWT_SECRET_KEY"] = "lflkf fklefl2rork"

app.register_blueprint(kupci_blueprint, url_prefix="/api/kupci")
app.register_blueprint(proizvodi_blueprint, url_prefix="/api/proizvodi")

mysql.init_app(app)

jwt = JWTManager(app)

@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route("/api/login", methods=["POST"])
def login():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s", flask.request.json)
    korisnik = cursor.fetchone()
    if korisnik is not None:
        access_token = create_access_token(identity=korisnik["korisnicko_ime"], additional_claims={"roles": ["USER"]})
        return flask.jsonify(access_token), 200
    return "", 403

@app.route("/api/logout", methods=["GET"])
def logout():
    session.pop("korisnik", None)
    return "", 200