export default {
    props: ["polise"],
    emits: ["izmena", "uklanjanje"],
    data() {
        return {}
    },
    computed: {
        aktivnePolise: function() {
            let aktivne = [];
            for(let p of this.polise) {
                if(new Date(p.datum_kraja) >  new Date()) {
                    aktivne.push(p);
                }
            }
            return aktivne;
        },
        isteklePolise: function() {
            let istekle = [];
            for(let p of this.polise) {
                if(new Date(p.datum_kraja) <=  new Date()) {
                    istekle.push(p);
                }
            }
            return istekle;
        }
    },
    template: `
<h1>Aktivne polise</h1>
<table>
<thead>
    <tr>
        <th>ID</th>
        <th>ID automobila</th>
        <th>ID osiguravajuce kuće</th>
        <th>Datum početka</th>
        <th>Datum kraja</th>
        <th>Cena</th>
    </tr>
</thead>
<tbody>
    <tr v-for="polisa in aktivnePolise">
        <td>{{polisa.id}}</td>
        <td>{{polisa.automobil_id}}</td>
        <td>{{polisa.osiguravajuca_kuca_id}}</td>
        <td>{{polisa.datum_pocetka}}</td>
        <td>{{polisa.datum_kraja}}</td>
        <td>{{polisa.cena}}</td>
        <td><button v-on:click="$emit('izmena', {...polisa})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', {...polisa})">Ukloni</button></td>
    </tr>
</tbody>
</table>
<h1>Istekle polise</h1>
<table>
<thead>
    <tr>
        <th>ID</th>
        <th>ID automobila</th>
        <th>ID osiguravajuce kuće</th>
        <th>Datum početka</th>
        <th>Datum kraja</th>
        <th>Cena</th>
    </tr>
</thead>
<tbody>
    <tr v-for="polisa in isteklePolise">
        <td>{{polisa.id}}</td>
        <td>{{polisa.automobil_id}}</td>
        <td>{{polisa.osiguravajuca_kuca_id}}</td>
        <td>{{polisa.datum_pocetka}}</td>
        <td>{{polisa.datum_kraja}}</td>
        <td>{{polisa.cena}}</td>
        <td><button v-on:click="$emit('izmena', {...polisa})">Izmeni</button><button
                v-on:click="$emit('uklanjanje', {...polisa})">Ukloni</button></td>
    </tr>
</tbody>
</table>
    `
}