import { AppOsiguravajucaKucaComponent } from "/components/app-osiguravajuca_kuca.js";
import { AppAutomobilComponent } from "/components/app-automobil.js";
import {AppPolisaComponent} from "/components/app-polisa.js"

Vue.component('app-osiguravajuca_kuca', AppOsiguravajucaKucaComponent);
Vue.component('app-automobil', AppAutomobilComponent);
Vue.component('app-polisa', AppPolisaComponent);

var app = new Vue({
    el: "#app",
    template:`
        <div>
            <h1>Nedelja 11 priprema za kolokvijum</h1>
            <app-osiguravajuca_kuca></app-osiguravajuca_kuca>
            <app-automobil></app-automobil>
            <app-polisa></app-polisa>
        </div>
    `,
    data:{
        
    }
})