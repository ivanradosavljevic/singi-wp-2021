"""
CREATE TABLE users (
    id int NOT NULL AUTO_INCREMENT,
    email varchar(255),
    password varchar(255),
    PRIMARY KEY (id)
);

pip install pymysql
"""

import pymysql.cursors

conn = pymysql.connect(
    host='localhost',
    user='root',
    password = 'levo.desno',
    database = 'web_programiranje',
    charset = 'utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

def primer_insert(conn):
    with conn.cursor() as cursor:
        sql = "INSERT INTO users (email, password) VALUES (%s, %s)"
        cursor.execute(sql, ('djobradovic@singidunum.ac.rs', 'kifla.krofna'))
    conn.commit()


def primer_select(conn):
    cursor = conn.cursor()
    sql = "SELECT id, email, password FROM users WHERE email=%s"
    cursor.execute(sql, ('djobradovic@singidunum.ac.rs',))
    result = cursor.fetchone()
    print(result)
    cursor.close()

def primer_update(conn):
    with conn.cursor() as cursor:
        sql = "UPDATE users SET password = %s WHERE email = %s"
        cursor.execute(sql, ('krofna.kifla', 'djobradovic@singidunum.ac.rs'))
    conn.commit()

def primer_delete(conn):
    with conn.cursor() as cursor:
        sql = "DELETE FROM users WHERE email = %s"
        cursor.execute(sql, ('djobradovic@singidunum.ac.rs',))
    conn.commit()

def primer_select_vise(conn):
    with conn.cursor() as cursor:
        sql = "INSERT INTO users (email, password) VALUES (%s, %s)"
        cursor.execute(sql, ('obrad@uns.ac.rs', '1234'))
    conn.commit()
    with conn.cursor() as cursor:
        sql = "SELECT id, email, password FROM users"
        cursor.execute(sql)
        for el in cursor.fetchall():
            print(el)


primer_insert(conn)
primer_select_vise(conn)


primer_select(conn)
primer_update(conn)
primer_select(conn)
# primer_delete(conn)
primer_select(conn)
