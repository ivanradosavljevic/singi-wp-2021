def pripremi_listu_studenata():
    studenti = [
        {'ime': 'Pera', 'prezime': 'Petrovic', 'index': 1234, 'ocene': [8, 9, 10]},
        {'ime': 'Milan', 'prezime': 'Markovic', 'index': 4321, 'ocene': [8, 8, 7]},
        {'ime': 'Marina', 'prezime': 'Markovic', 'index': 4321, 'ocene': [10, 9, 10]}
    ]
    return studenti

