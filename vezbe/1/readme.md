# Vežbe 1
*Cilj vežbi: Obnavljanje HTML-a, CSS-a i JavaScript-a. Podešavanje razvojnog okruženja i pravljenje jednostavnog aplikativnog web servera.*

**Preduslovi:** Instaliran VisualStudio Code, instaliran Python interpreter.

## Zadaci
1. Napraviti HTML stranicu sa tabelom za prikaz podataka o studentima. Podaci koje treba prikazati su: broj indeksa, ime, prezime i prosečna ocena. Stilizovati tabelu tako da su redovi i kolone odvojeni crnim linijama a redovi naizmenično obojeni sivom i belom bojom. Stil smestiti u odvojenu datoteku.
2. Napraviti HTML stranicu za dodavanje podataka o studentima. Stranica treba da sadrži formu sa poljima za unos indeksa, imena, prezimena i prosečne ocene. Onemogućiti unos prosečne ocene manje od 5 ili izostavljanje vrednosti u nekom od prethodno navedenih polja. Prilikom predaje sadržaja forme izvršiti validaciju broja indeksa i u slučaju neispravno unetog indeksa ispisati poruku o grešci. Format indeksa mora biti GGGG/BBBBBB, pri čemu GGGG predstavlja godinu upisa a BBBBBB šestocifreni broj koji identifikuje studenta upisanog navedene godine.
3. Napraviti i aktivirati virtualno okruženje. Instalirati Flask radni okvir u napravljeno okruženje.
    ### Uputstvo:
    1. Kreiranje virtualnog okruženja:
        ```cmd
        > python -m venv putanja\do\okruženja\naziv-okruženja
        ```
    2. Aktiviranje kreiranog virtualnog okruženja:
        ```cmd
        > putanja\do\okruženja\naziv-okruženja\scripts\activate
        ```
    3. Instaliranje Flask radnog okvira u aktiviranom okruženju:
        ```cmd
        (naziv-okruženja) > python -m pip install flask
        ```
4. Napraviti web aplikacij koja servira prethodno napravljene stranice. Kao početnu stranicu postaviti stranicu sa tabelom studenata. Na ruti /dodavanje servirati stranicu sa formom za dodavanje studenata. Pokrenuti web aplikaciju i testirati rešenje.
    ### Uputstvo:
    1. Podešavanje sistemskih promenljivih:
        ```cmd
        (naziv-okruženja) > SET FLASK_APP=main.py
        (naziv-okruženja) > SET FLASK_ENV=development
        ```
    2. Pokretanje web aplikacije:
        ```cmd
        (naziv-okruženja) > flask run
        ```
5. Prepraviti rešenje tako da se serviranje statičkog sadržaja vrši direktno iz datoteke pod nazivom static. Pokrenuti web aplikaciju i testirati rešenje.
___
### Dodatne napomene:
* Dokumentacija za Flask radni okvir: https://palletsprojects.com/p/flask/.
___