export var AppDrzavaComponent = {

    template:`
    <div class="drzava">
        <h1> Drzava </h1>
        <button v-on:click="create()">Create</button>
        <button v-on:click="update()">Update</button><br>
        <form>
            <input v-model="izabrana_drzava.naziv">
        </form>
        <div v-for="drzava in drzave">
            <a href="#" v-on:click="select(drzava)">{{drzava.naziv}}</a>
            <a href="#" v-on:click="delete_drzava(drzava.id)">delete</a>
        </div>

        <app-mesto v-bind:drzava="izabrana_drzava"></app-mesto>

    </div>
    `,
    data:function(){
        return {
            drzave:[],
            izabrana_drzava:{
                naziv:''
            }
        }
    },
    created:function(){
        this.read();
    },
    methods:{
        select:function(drzava){
            this.izabrana_drzava = {...drzava}
        },
        create:function(){
            axios.post("/api/drzava", this.izabrana_drzava).then(
                response =>{
                    this.read();
                } 
            )
        },
        read:function(){
            axios.get("/api/drzava").then(
                response =>{
                    this.drzave = response.data;
                    this.izabrana_drzava = {...this.drzave[0]};
                } 
            )
        },
        update:function(){
            axios.put("/api/drzava/"+this.izabrana_drzava.id, this.izabrana_drzava).then(
                response =>{
                    this.read();
                } 
            )
        },
        delete_drzava:function(id){
            axios.delete("/api/drzava/"+id).then(
                response =>{
                    this.read();
                } 
            )
        }
    }

}