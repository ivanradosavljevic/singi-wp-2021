import Osiguranja from './components/osiguranja.js';
import TabelaAutomobila from './components/tabelaAutomobila.js';
import TabelaPolisa from './components/tabelaPolisa.js';
import TabelaOsigravajucihKuca from './components/tabelaOsiguravajucihKuca.js';
import AutomobilForma from './components/automobilForma.js';
import OsiguravajucaKucaForma from './components/osiguravajuceKuceForma.js';
import PolisaForma from './components/poliseForma.js';

const app = Vue.createApp(Osiguranja);
app.component('tabela-automobila', TabelaAutomobila);
app.component('tabela-polisa', TabelaPolisa);
app.component('tabela-osiguravajucih-kuca', TabelaOsigravajucihKuca);

app.component('automobil-forma', AutomobilForma);
app.component('osiguravajuca-kuca-forma', OsiguravajucaKucaForma);
app.component('polisa-forma', PolisaForma);
app.mount("#app");