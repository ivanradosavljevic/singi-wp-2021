import flask
from flask import Flask
from flaskext.mysql import MySQL
from flaskext.mysql import pymysql

app = Flask(__name__, static_url_path="/")
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "prodavnica"

mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route("/api/kupci")
def get_all_kupci():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac")
    kupci = cursor.fetchall()
    return flask.jsonify(kupci)

@app.route("/api/kupci/<int:kupac_id>")
def get_kupac(kupac_id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac WHERE id=%s", (kupac_id,))
    kupac = cursor.fetchone()
    if kupac is not None:
        return flask.jsonify(kupac)
    
    return "", 404

@app.route("/api/kupci", methods=["POST"])
def dodavanje_kupca():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupac(korisnickoIme, lozinka, ime, prezime) VALUES(%(korisnickoIme)s, %(lozinka)s, %(ime)s, %(prezime)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@app.route("/api/kupci/<int:kupac_id>", methods=["PUT"])
def izmeni_kupca(kupac_id):
    kupac = dict(flask.request.json)
    kupac["kupac_id"] = kupac_id
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE kupac SET korisnickoIme=%(korisnickoIme)s, lozinka=%(lozinka)s, ime=%(ime)s, prezime=%(prezime)s WHERE id=%(kupac_id)s", kupac)
    db.commit()
    cursor.execute("SELECT * FROM kupac WHERE id=%s", (kupac_id,))
    kupac = cursor.fetchone()
    return flask.jsonify(kupac)

@app.route("/api/kupci/<int:kupac_id>", methods=["DELETE"])
def ukloni_kupca(kupac_id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM kupac WHERE id=%s", (kupac_id, ))
    db.commit()
    return ""